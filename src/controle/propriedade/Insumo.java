/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle.propriedade;

/**
 *
 * @author Mauricio
 */
public class Insumo {
    
    private int idInsumo;
    private String descrição;
    private float diluição;
    private String nome;
    private int idTipo;

    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    public int getIdInsumo() {
        return idInsumo;
    }

    public void setIdInsumo(int idInsumo) {
        this.idInsumo = idInsumo;
    }

    public String getDescrição() {
        return descrição;
    }

    public void setDescrição(String descrição) {
        this.descrição = descrição;
    }

    public float getDiluição() {
        return diluição;
    }

    public void setDiluição(float diluição) {
        this.diluição = diluição;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public int salvarInsumo(Insumo insumo) {

        ConexaoMySql conexao = new ConexaoMySql();

        try {

            conexao.conectar();

            String sql
                    = "INSERT INTO Insumo (descricao,diluicao,nome,idTipo) VALUES ("
                    + "'" + insumo.getDescrição()+ "',"
                    + "'" + insumo.getDiluição()+ "'"
                    + "'" + insumo.getNome()+ "'"
                    + "'" + insumo.getIdTipo()+ "'"
                    + ");";

            return conexao.insertSQL(sql);

        } catch (Exception e) {
            return 0;
        } finally {
            conexao.fecharConexao();
        }
    }

}
