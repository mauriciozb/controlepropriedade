/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle.propriedade;

/**
 *
 * @author Mauricio
 */
public class Cliente {
    
    private int idCliente;
    private String nome;
    private String cidade;
    private String telefone;
    private String telefoneCelular;

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }
    

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getTelefoneCelular() {
        return telefoneCelular;
    }

    public void setTelefoneCelular(String telefoneCelular) {
        this.telefoneCelular = telefoneCelular;
    }

   
    
    
    

    public void cadastraCliente(String nome,String telefone,String telefoneCelular){
        Cliente cliente = new Cliente();
        cliente.idCliente++;
        cliente.nome=nome;
        cliente.telefone=telefone;
        cliente.telefoneCelular=telefoneCelular;
    }
    
    public int salvarCliente(Cliente cliente) {

        ConexaoMySql conexao = new ConexaoMySql();

        try {

            conexao.conectar();

            String sql
                    = "INSERT INTO Cliente (nome, telefone,telefoneCelular,cidade) VALUES ("
                    + "'" + cliente.getNome() + "',"
                    + "'" + cliente.getTelefone()+ "'"
                    + "'" + cliente.getTelefoneCelular()+ "'"
                    + "'" + cliente.getCidade()+ "'"
                    + ");";

            return conexao.insertSQL(sql);

        } catch (Exception e) {
            return 0;
        } finally {
            conexao.fecharConexao();
        }
    }

    
    
}


